const puppeteer = require('puppeteer');
const express = require('express');
const smoothCode = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Anecdote = require('./models/anecdote');
const { title } = require('process');

mongoose.connect('mongodb+srv://PBBM:351426@cluster0.lxbk2.mongodb.net/DbTestAnecdotes?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
        .then(() => console.log('Connexion à MongoDB réussie !'))
        .catch(() => console.log('Connexion à MongoDB échouée !'));


// Utilisation du Body Parser pour utilisation dans les requêtes en JSON
smoothCode.use(bodyParser.json());

//================================================================================================================================
//================================ PARTIE SERVER =================================================================================
//================================================================================================================================
smoothCode.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

    const saveData = async(descriptionToSave) => {
        const anecdote = new Anecdote(
            {
                description: await descriptionToSave
            })
        anecdote.save();
    }

//========================   Post - OK   =================================================
// smoothCode.post('/api/stuff', (req, res, next) => {
//     const anecdote = new Anecdote({
//         ...req.body
//     });
//     anecdote.save()
//         .then(() => res.status(201).json({ message: 'Objet enregistré ! '}))
//         .catch(error => res.status(400).json({error}));
// });
//========================================================================================

// ========================   Get All - OK   ==============================================
// smoothCode.get('/api/stuff', (req, res, next) => {

//    console.log("get test");

//    scrap()
//     .then(value => {
//         res.status(200).json(value)
//     })
//     .catch(e => res.status(400).json(`error: ${e}`))

    // Anecdote.find()
    //     .then(anecdotes => res.status(200).json(anecdotes))
    //     .catch(error => res.status(400).json({ error }))
// });
// ========================================================================================

//========================   Get One by Id - OK   ========================================
// smoothCode.get('/api/stuffById/:id', (req, res, next) => {
//     Anecdote.findOne({ _id : req.params.id })
//         .then(anecdote => res.status(200).json(anecdote))
//         .catch(error => res.status(404).json({ error }))
// });
//========================================================================================

//========================   Get One by Title - OK   =====================================
// smoothCode.get('/api/stuffByTitle/:title', (req, res, next) => {
//     let titleSearched = req.params.title;
//     Anecdote.find({"title" : {$regex : ".*"+titleSearched+".*", $options: 'i'} }) // regex = regular expression and 'i' for case insensitivity
//         .then(anecdotes => res.status(200).json(anecdotes))
//         .catch(error => res.status(400).json({ error }))
// });
//========================================================================================

//========================   Get One by Description content - OK   =======================
// smoothCode.get('/api/stuffByDescription/:description', (req, res, next) => {
//     Anecdote.find({"description" : {$regex : ".*"+req.params.description+".*", $options: 'i'} })
//         .then(anecdotes => res.status(200).json(anecdotes))
//         .catch(error => res.status(400).json({ error }))
// });
//========================================================================================
//================================================================================================================================



//================================================================================================================================
//===================================== PARTIE BDD== =============================================================================
//================================================================================================================================
    // const anecdoteTest = new Anecdote({
    //     title: 'title test',
    //     description: "description test"
    // });
    // anecdoteTest.save();
//================================================================================================================================
//================================================================================================================================




//================================================================================================================================
//===================================== PARTIE SCRAP =============================================================================
//================================================================================================================================


const getAllUrlCurrentPage = async (page) => {
    const result = await page.evaluate(() =>
        [...document.querySelectorAll(".summary a")].map(link => link.href),
    )
    return result;
}

const nextClickFunction = async (page) => {
    try {
        await page.waitForSelector(".read-more");
        await page.click("#main-content > div > ul > li.next > a");
    }
    catch (err) {
        console.log("ANOMALIE AU NIVEAU DE LA FONCTION click" + err);
    }
}

// const nextScrapFunction = async (page, dataFirstScrap) => {
//     try {
//         nextClickFunction(page);
//         await page.waitForTimeout(6000)
//         let dataSecondScrap = await getAllUrlCurrentPage(page);
//         let fullData = await dataFirstScrap.concat(dataSecondScrap);
//         return fullData;
//     }
//     catch (err) {
//         console.log("ANOMALIE AU NIVEAU DE LA FONCTION nextScrap" + err);
//     }
// }



// =======================================   Get data    ===========================================================
const getData = async (linkForData) => {

    // 1 - Créer une instance de navigateur
    const browser = await puppeteer.launch({ headless: true })
    const page = await browser.newPage()

    await page.goto(linkForData)

    await page.waitForSelector(".summary") // fait une pause de +/-4 secondes

    try {
        // 3 - Récupérer les données
        const result = await page.evaluate(() => {

            let resume = document.querySelector(".summary").innerText // ".anecdote" ou ".summary"
            return { resume }
        })

        // 4 - Retourner les données (et fermer le navigateur)
        browser.close()
        return result
    }
    catch (err) {
        console.log(err);
    }
}
// ================================================================================================================


// ======================================= Get all links ============================================================================
const getAllUrls = async () => {
    const browser2 = await puppeteer.launch({ headless: true })
    const page = await browser2.newPage()
    await page.goto('https://secouchermoinsbete.fr/')
    await page.waitForSelector(".read-more")

    let dataFirstScrap = await getAllUrlCurrentPage(page);
    let fullData = dataFirstScrap;
    for (let i = 0; i < 0; i++) {
        nextClickFunction(page);
        await page.waitForTimeout(5000)

        let dataCurrentScrap = await getAllUrlCurrentPage(page);

        fullData = await fullData.concat(dataCurrentScrap);
    }

    return fullData;
}
//==================================================================================================================================




//==================================================================================================================================
const scrap = async () => {
    const browser = await puppeteer.launch({ headless: true})
    const urlList = await getAllUrls()
    const results = await Promise.all(
        urlList.map(url => getData(url)),
    )
    return results
}
//==================================================================================================================================





//================ Lancement de la fonction principale du scrapper  ================================
scrap()
  .then(value => {
    console.log("=============================")
    console.log("Anecdotes ajoutées ce jour : ")
    console.log("=============================")
    for (let i = 0; i < 20; i++) {
        console.log(value[i].resume);
        saveData(value[i].resume);
        console.log(" ");
    }
  })
  .catch(e => console.log(`error: ${e}`))
//==================================================================================================

module.exports = smoothCode;